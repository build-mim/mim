\newtheorem{definition}{Definition}

\documentclass{article}
\usepackage{graphics}
\usepackage{epsfig}
\usepackage{times}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{url,hyperref}



\author{[YOUR NAME]\\
\texttt{[YOUR EMAIL]}
}
\title{Implications of an implicit construction tool}
\begin{document}
\maketitle

\section{Abstract}
In this paper we describe a new concept -- \emph{implicit construction}  --in the area of software construction (commonly referred to as \emph{build systems}).

We also describe \emph{Mim} -- a build system substrate that implements this concept. We show how Mim simplifies complex build systems and remove manual dependency management entierly. Furthermore, Mim also offers more oportunities for parallelism, while reducing the complexity of process management.

\section{Introduction}
Even though computers are used for many tasks, suprisingly few of those tasks are automated correctly with regards to dependencies. Similarly, scripts are often used for automation of tasks, however, such scripts usually redo all work even though some part has does not need to be redone.

Manually managing dependencies is not feasible for any software of practical value. Thus, software construction utilities\footnote{The computer application that assists in the automatic building of executable programs, etc., from source code. Examples of such tools are GNU make and SCons.}, \emph{SCU}, rely on dependecy scanning to find dependencies, e.g., \texttt{gcc} can be used for finding dependencies of C/C++ source code. 

Implementing such scanner are not a simple task, however. Additionally, by its nature a scanner only works for a specific kind of file, e.g., C++ source code. Thus, a multitude of scanners is needed for most practial purposes. We argue that the dependency scanning technique is simply not good enough in todays complex build systems.

This paper describes an alternative way of finding, managing, and handle dependencies. It is fully automated without need for file-specific dependency scanning. A prototype called \emph{Mim} is implemented that indicates that builds with complex dependencies (involving several layers of code generation, C libraries, headers, etc.) is as simple to implement as the equivalent shell command.

\section{Software Construction Utilities}
In addition to find and manage dependencies and build artifacts, a SCU has to be fast (finish a build in short time) and reliable (everything that needs to be updated is updated). In order to do this, the SCU needs to know every dependency of every artifact\footnote{We call a file that is build by the SCU an \emph{artifact}.}.

\subsection{Dependency graph}
The heart of a SCU is its ability to construct a \emph{DAG}, a directed acyclic graph, describing the artifacts and their dependencies. Constructing such graph is not trivial, though, due to implicit dependencies, such as \texttt{\#include} in C programs.

However, for anything more complex than simply compiling traditional source code, or other common tasks, you will likely need to explicitly declare all dependencies because there is no dependency scanner available, e.g., if dependencies are embedded in source code comments. Similarly, deriving the dependencies of a program that loads libraries dynamically using \texttt{dlopen} is generaly imposible using dependency scanning\footnote{This would require the dependency scanner to derive the argument to \texttt{dlopen}, which equivalent to solving the \emph{entscheidungsproblem}.}.

\section{Construction tools vs. scripting languages}
Most construction tools invent their own language in which the build has to be expressed. These languages are often ill-suited for everything other than expressing build commands and dependencies.
What construction tools languages have that general-purpose languages do not have is the ability to only invoke a function if necessary. For instance, assume we have the following code in a language designed for construction tools:
\begin{verbatim}
   artifact.txt = execute(echo "hello" > artifact.txt)
\end{verbatim}
The language would enforce that \texttt{execute} is only invoked to build \texttt{artifact.txt} if it needs updating. If \texttt{artifact.txt} already up-to-date (because it was updated when the construction tool was last run), then no action is performed. General-purpose languages, on the other hand, do not have this feature natively.

Having said that, few of us would use a construction tool language for anything else than expressing build-command and dependencies. In fact, many of use would gladly use a general-purpose language for this activity, which may explain the popularity of Scons.


\section{Theory}
\label{section: Theory}
The contents of an artifact $A$ is defined as $C_A = f_A(D_A)$, where $f_A$ is the function\footnote{Often a shell command or similar.} that reads the set of dependencies $D_A$ and produces the file contents $C_A$. This implies that $A$ needs to be build iff $f_A$, or $D_A$ change. In other words, $f_A$ and $D_A$ are $A$s dependencies

We say that an artifact's contents, $C_A$, is \emph{implicitly constructed} iff $f_A(D_A)$ is invoked automatically as a result of $A$ being read. However, in order to \emph{read} $A$ it has to exists, and experience with traditional construction tools tells us that files (artifacts) do not exists before that have been built.

Hence, it follows that an \emph{implicit} construction tool behaves differently in this regard. Such tool don't create the artifact when it builds it; it only updates it's contents. In other words, in an environment with an implicit SCU, an artifact always exists but it's contents might not.

\section{Parallelism Opportunities}
The way an implicit SCU works implices that it offeres greater possibilities for concurrency. Consider a source tree with the DAG depicted in figure \ref{dag}. A traditional SCU would maximize concurrency by building this tree as described in figure.
\begin{figure}[h]
  \begin{center}
%    \includegraphics[bb = 0 430 350 560, scale=0.5]{figure.pdf}
  \end{center}
  \caption{\small The DAG describing the depedencies of seven files, e.g., $A$ depends on $B$ depends on $D$.}
  \label{dag}
\end{figure}


\section{Implementation}
As discussed in section \ref{section: Theory}, the requirements of a implicit construction tool are:
\begin{itemize}
  \item updating an artifact (if needed) when the artifact is read, and
  \item making (unbuilt) artifacts being listed in directories as normal files.
\end{itemize}
We implement the \emph{implicit construction} concepts as a thin layer on-top the filesystem; we call this thin layer \emph{Mim}. Mim intercepts filesystems calls, e.g, the \texttt{open} and \texttt{opendir} call to implement the above requirements. In contrast to traditional construction tools, \emph{Mim} is always running in the background. This is a result of having to intercept the filesystem calls and act on them.

In addition to this, Mim has mechanisms for adding and removing artifacts. This is similar to editing a \texttt{Makefile} and add/remove a line describing a new artifact (or "target" as \texttt{make} calls it). 

We now illustrate how this works using an example. In this example Mim is running and there is one files in the Mim directory: the C source code of a Hello World program.
\subsubsection*{Example \#1}
\begin{verbatim}
  $ ls
  helloworld.c
  $ mim add -x helloworld gcc helloworld.c -o helloworld
  $ ls
  helloworld  helloworld.c
  $ ./helloworld
  Hello world! 
\end{verbatim}
Here, we first list the files in the directory, then an executable artifact is added and it's build command is specified. After this artifact is added it can be seen in the directory listing. Finally, when the command to execute the artifact is given, it is first implicitly built, then executed.

\subsection{Order of execution}
A traditional SCU builds the leafs of the DAG firsts. Mim, however, has at a first glance a more complex order of execution, but it actually corresponds directly to function calls in programming languages like C.

Consider the example in figure [myprint]. A traditional SCU builds \texttt{hello.o} or \texttt{myprint.o} first, and then the executable \texttt{hello}. Mim, on the other hand, first \emph{starts} to build \texttt{hello}, but \emph{pauses} when it reads \texttt{hello.o}. It then builds \texttt{hello.o} and when it finishes it \emph{resumes} building \texttt{hello}. Similarly, \texttt{hello} is paused when it reads \texttt{myprint.o}. Mim builds \texttt{myprint.o} and resumes building{hello}. Mim then finishes building \texttt{hello}. This is illustrated in figure [function call metaphor].


\subsection{Error handling}
When using an implicit construction tool to build files, it is not obvious how build error should be presented to the user. For instance, consider an syntax error in \texttt{helloworld.cc} in Example \#1. How to display the error messages from the compiler to the user?

The approach taken by Mim is to overload\footnote{Using \texttt{LD\_PRELOAD}, for instance.} the standard C function \texttt{strerror}, which maps an error code to a textual description. This is the pseudo-code for such function:
\begin{verbatim}
char const* strerror(int errnum) {
  if (errnum & MIM_ERROR_MASK)
    return mim_strerror(errnum);
  else
    return std_strerror(errnum);
}
\end{verbatim}
where \texttt{mim\_strerror} is a function that returns the error messages outputted by the build-command. The function \texttt{std\_strerror} is the original function that was overloaded.

Using this approach it is possible to give good error messages when artifacts failes to build, even though they are built implicitly. 

\subsection{Performance}
Since Mim finds the dependencies of an artifact when the artifact is built, Mim does not have a DAG describing the dependencies before it is built, thus, Mim cannot build it concurrently with other artifacts the first it is built, which limits the performance of Mim significantly when building an entire source tree from scratch (e.g., after restarting Mim).

However, it is straight-forward to write the DAG to disk, thus, there is always a reasonable up-to-date DAG available. Thus, it should rarely be needed to rebuild the entire source tree from scratch. In other words, this problem is not an inherent problem with implicit construction tool; it is just a limitation of the current implementation of Mim.


\section{Conclusion}
\end{document}
