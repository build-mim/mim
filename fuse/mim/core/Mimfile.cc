#include "Mimfile.hh"
#include <unistd.h>
#include <set>
#include <fstream>
#include <boost/algorithm/string/replace.hpp>

extern "C" {
#include "lua5.1/lua.h"
#include "lua5.1/lualib.h"
#include "lua5.1/lauxlib.h"
}

namespace mim {

static const int LS_NUM_ARGS = 0;
static const int LS_NUM_RETURNS = 1;

Mimfile::Mimfile(bfs::path physicalFilename, bfs::path mountPoint,
                 bfs::path storagePoint, bfs::path virtualDir) :
    mimfileName(physicalFilename),
    mountPoint(mountPoint),
    storagePoint(storagePoint),
    virtualDir(virtualDir) {
  openMimfile();
}
    

std::string Mimfile::symbolicCmdToRealCmd(std::string cmd, bfs::path filename) {
    boost::algorithm::replace_all(cmd, "%output", (storagePoint / virtualDir / filename).string());
	return "cd " + mountPoint.string() + " && " + cmd;
}

void Mimfile::openMimfile() {
  struct ::stat defaultAttribs;
  defaultAttribs.st_ctime = 1274217618;
  defaultAttribs.st_mtime = 1274194338;
  defaultAttribs.st_nlink = 1;
  defaultAttribs.st_mode = 33188;
  defaultAttribs.st_size = 10000;
  defaultAttribs.st_gid = 1000;
  defaultAttribs.st_uid = 1000;
  defaultAttribs.st_atime = 1274194338;


  if (0 != access(mimfileName.string().c_str(), R_OK))
    return;

  lua_State* luaState = lua_open();
  luaL_openlibs(luaState);
  luaL_dofile(luaState, mimfileName.string().c_str());

  lua_getglobal(luaState, "ls");
  lua_call(luaState, LS_NUM_ARGS, LS_NUM_RETURNS);
  
  std::set<bfs::path> previousFiles(artifactFiles.begin(), artifactFiles.end());
  artifactFiles.clear();
  
  for (lua_pushnil(luaState); lua_next(luaState, -2); lua_pop(luaState, 1)) {
    std::vector<std::string> list;
    for (lua_pushnil(luaState); lua_next(luaState, -2); lua_pop(luaState, 1)) {
      list.push_back(lua_tostring(luaState, -1));
    }
    bfs::path filename = list[0];
    previousFiles.erase(filename);
	std::string symCmd = list[1];

	newArtifact(filename,
	            symbolicCmdToRealCmd(symCmd, filename),
	            defaultAttribs);
  }
  
  for (std::set<bfs::path>::iterator it = previousFiles.begin(); it != previousFiles.end(); ++it) {
    unlink((storagePoint / it->filename()).string().c_str());
    artifacts.erase(*it);
  }
  
  lua_close(luaState);
}

Filenames Mimfile::files() {
  return artifactFiles;
}

Artifact& Mimfile::artifactNamed(bfs::path filename) {
  return artifacts[filename];
}

void Mimfile::refresh() {
  openMimfile();
}


int Mimfile::rename(bfs::path oldname, bfs::path newname) {
  Artifact& a = artifactNamed(oldname.filename());
  a.rename(newname);

  std::ofstream file(mimfileName.string().c_str());
  file << "function ls()" << std::endl
       <<  "  return {" << std::endl;
  for (ArtifactMap::iterator it = artifacts.begin(); it != artifacts.end(); ++it)
    file << "    { \"" << it->second.filename() << "\", \"" << it->second.command() << "\" }," << std::endl;

  file << "  }" << std::endl
       << "end" << std::endl;
  file.close();
  int result = ::rename((mimfileName.parent_path() / oldname).string().c_str(),
                        (mimfileName.parent_path() / newname).string().c_str());
  refresh();
  return result;
}

void Mimfile::newArtifact(bfs::path const& filename,
                          std::string const& command,
                          struct ::stat const& attribs) {
  artifactFiles.push_back(filename);
  artifacts[filename] = Artifact(virtualDir / filename, command, attribs);
}


}
