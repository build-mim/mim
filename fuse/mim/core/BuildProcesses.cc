#include "BuildProcesses.hh"

#include "ArtifactFiles.hh"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h> 
#include <proc/readproc.h>
#include <tbb/concurrent_hash_map.h>

namespace mim {
  
pid_t shell(std::string cmd, int stdout, int stderr) {
  pid_t pid = fork();
  if (pid == 0) {
    dup2(stdout, STDOUT_FILENO);
    dup2(stderr, STDERR_FILENO);
    int i = execl("/bin/sh", "sh", "-c", cmd.c_str(), (const char*)0); // A successful call to execl does not return.
    printf("Failed to start '%s'. execl returned %d\n", cmd.c_str(), i);
    _exit(0); // Exit this fork:ed process.
  } else if (pid > 0) {
    return pid;
  } else {
    printf("Really bad! Failed to fork, error = %d\n", errno);
    return 0;
  }
}
  
  
class BuildProcess {
  pid_t myPid;
  Artifact* artif;
  pid_t parentPid;

public:
  BuildProcess() : myPid(0), artif(0), parentPid(0) { }
  BuildProcess(pid_t myPid, Artifact& artifact, pid_t parentPid) :
    myPid(myPid), artif(&artifact), parentPid(parentPid) { }
  
  pid_t parent() const { return parentPid; }
  std::string command() const { return artif->command(); }
  Artifact& artifact() const { return *artif; }
};
  
struct PidCompare {
  static size_t hash(const pid_t& p) { return p; }
  static bool equal(const pid_t& x, const pid_t& y) { return x == y; }
};

class ProcessesMap : public tbb::concurrent_hash_map<pid_t, BuildProcess, PidCompare> { };
  
BuildProcesses::BuildProcesses() : processes(*new ProcessesMap()) {
    
}
  
BuildProcesses::~BuildProcesses() {
  delete &processes;
}

bool BuildProcesses::isBuildProcess(pid_t pid) {
  ProcessesMap::accessor acc;
  return processes.find(acc, pid);
}


pid_t _parentOf(pid_t pid) {
  proc_t process_info;
  get_proc_stats(pid, &process_info);
  return process_info.ppid;
}

pid_t BuildProcesses::buildProcessThatStarted(pid_t pid) {
  while (pid != 0) {
    if (isBuildProcess(pid))
      return pid;
    pid = _parentOf(pid);
  }
  return -1;
}

Artifact& BuildProcesses::artifactBeingBuiltBy(pid_t pid) {
  ProcessesMap::const_accessor acc;
  if (processes.find(acc, pid))
    return acc->second.artifact();
  assert(false && "No artifact exists for given process id.");
}

int BuildProcesses::executeProcessFor(Artifact& artifact, pid_t parentPid, int stdout, int stderr) {
  int status = 0;
  pid_t pid = shell(artifact.command(), stdout, stderr);
  {
    ProcessesMap::accessor acc;
    processes.insert(acc, pid);
    acc->second = BuildProcess(pid, artifact, parentPid);
  }
  
  waitpid(pid, &status, 0);
  processes.erase(pid);
  return status;
}



}
