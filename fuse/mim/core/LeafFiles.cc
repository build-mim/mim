#include "LeafFiles.hh"
#include "Artifact.hh"
#include <tbb/concurrent_hash_map.h>

namespace mim {


  
struct PathCompare {
  static size_t hash(const bfs::path& p) {
    size_t h = 0;
    std::string x = p.string();
    for (const char* s = x.c_str(); *s; ++s)
      h = (h * 17) ^ *s;
    return h;
  }

  static bool equal(const bfs::path& x, const bfs::path& y) {
    return x == y;
  }
};

class DependenciesMap : public tbb::concurrent_hash_map<bfs::path, ArtifactSet, PathCompare> { };


LeafFiles::LeafFiles() : dependencies(*new DependenciesMap()) { }

LeafFiles::~LeafFiles() {
  delete &dependencies;
}


void LeafFiles::modified(bfs::path filename) {
  DependenciesMap::accessor da;
  dependencies.insert(da, filename);
  ArtifactSet& set = da->second;
  for (ArtifactSet::iterator it = set.begin(); it != set.end(); ++it)
    it->first->makeNotUpdated();
}


void LeafFiles::defineDependency(Artifact& requester, bfs::path filename) {
  DependenciesMap::accessor da;
  dependencies.insert(da, filename);
  ArtifactSet& set = da->second;
  
  ArtifactSet::const_accessor aa;
  set.insert(aa, &requester);
}

}