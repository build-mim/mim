#ifndef MIM_BUILD_DEPENDENCIES
#define MIM_BUILD_DEPENDENCIES

#include "Artifact.hh"
#include <boost/filesystem/path.hpp>

namespace mim {

namespace bfs = boost::filesystem;
class DependenciesMap;

class LeafFiles {
  
  DependenciesMap& dependencies;
   
  LeafFiles(const LeafFiles&);
  LeafFiles& operator=(const LeafFiles&);
  
public:
  LeafFiles();
  ~LeafFiles();
  
  void modified(bfs::path filename);
  
  void defineDependency(Artifact& requester, bfs::path path);

};
  
}

#endif