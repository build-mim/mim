#ifndef MIM_BUILD_PROCESSES
#define MIM_BUILD_PROCESSES

#include <sys/types.h>

namespace mim {
  
  
class ProcessesMap;
class Artifact;

class BuildProcesses {
  ProcessesMap& processes;
  
  BuildProcesses(const BuildProcesses&);
  BuildProcesses& operator=(const BuildProcesses&);
  
  bool isBuildProcess(pid_t pid);
public:
  BuildProcesses();
  ~BuildProcesses();
  
  //! Execute the command line for 'artifact' and wait for termination. Returns
  //! the exit status of the executed process.
  int executeProcessFor(Artifact& artifact, pid_t parentPid, int stdout, int stderr);

  //! Get the pid of the build process that started the process with pid 'pid'.
  pid_t buildProcessThatStarted(pid_t pid);

  //! Get the artifact that is built by the build process with pid 'pid'
  Artifact& artifactBeingBuiltBy(pid_t pid);
}; 
  
}

#endif
