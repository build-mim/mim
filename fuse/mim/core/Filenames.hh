#ifndef MIM_COMMON_TYPES
#define MIM_COMMON_TYPES

#include <boost/filesystem/path.hpp>
#include <vector>

namespace mim {

class Filenames : public std::vector<boost::filesystem::path> { };

}

#endif
