#include "ArtifactFiles.hh"
#include "Serializer.hh"
#include "CommandInterface.hh"
#include "Mim.hh"
#include <assert.h>
#include <boost/interprocess/ipc/message_queue.hpp>
#include <pthread.h>

namespace mim {
namespace bi = boost::interprocess;
namespace bfs = boost::filesystem;


class MessageHandler {
  ArtifactFiles& artifacts;
  char buffer[1024];
public:
  MessageHandler(ArtifactFiles& artifacts) : artifacts(artifacts) { }

  void handle(messages::ReqArtifactInfo const& req) {
    size_t used = 0;
    if (artifacts.exists(req.path)) {
      Artifact& artifact = artifacts.artifactNamed(req.path);
      messages::RspArtifactInfo rsp = { artifact.command(),
                                        artifact.stdoutFilename().string(),
                                        artifact.stderrFilename().string() };
      used = ServerToClient::encode(rsp, buffer);
	} else {
      messages::RejArtifactInfo rej = { req.path };
      used = ServerToClient::encode(rej, buffer);
    }

     bi::message_queue mq(bi::open_only,
                          req.queue.c_str()); // Max message size.

    mq.send(buffer, used, 0);
  }
};


class MessageQueueReader {
  char const* buffer;
  size_t size;
public:
  MessageQueueReader(char const* buffer, size_t size) : 
    buffer(buffer), size(size) { }

  void operator()(void* dest, size_t size) {
    memcpy(dest, buffer, size);
    buffer += size;
  }
};



//! Get a string that is per-process-unique.
std::string messageQueueName() {
   char buffer[64];
   snprintf(buffer, 64, "mim-server-%d", getpid());
   return buffer;
}

void* commandInterfaceMain(void* arg) {
  Mim& mim = *reinterpret_cast<Mim*>(arg);
  
  // TODO: The queue name must handle more than one mim instance on the same machine.
  std::string queueName = messageQueueName();
  printf("\n\n ------------ message queue name: %s ------------\n\n\n", queueName.c_str());
  try {
     // Erase any previous message queue
     bi::message_queue::remove(queueName.c_str());

     // Create a message_queue.
     bi::message_queue mq(bi::create_only,
                          queueName.c_str(),
                          1,            // Max message number.
                          1024); // Max message size.
   
    char buffer[1024];
    size_t receivedSize;
    unsigned int priority;
    MessageHandler handler(mim.artifactFiles());
    while (true) {
      mq.receive(buffer, 1024, receivedSize, priority);
      MessageQueueReader reader(buffer, receivedSize);
      ClientToServer::decode(handler, reader);
    }

  } catch (bi::interprocess_exception& ex){
    printf("Error in command interface: %s\n", ex.what());
  }
  bi::message_queue::remove(queueName.c_str());
  return 0;
}

void startCommandInterface(Mim& mim) {
  pthread_t thread1;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
//  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED); // Will never do join on the thread.
  pthread_create(&thread1, &attr, commandInterfaceMain, &mim);
  pthread_attr_destroy(&attr);
}

}
