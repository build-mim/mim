#include "ArtifactFiles.hh"

#include <vector>
#include <string>
#include <algorithm>
#include <tbb/concurrent_hash_map.h>

namespace mim {


struct PathCompare {
  static size_t hash(const bfs::path& p) {
    size_t h = 0;
    std::string x = p.string();
    for (const char* s = x.c_str(); *s; ++s)
      h = (h * 17) ^ *s;
    return h;
  }

  static bool equal(const bfs::path& x, const bfs::path& y) {
    return x == y;
  }
};

class MimfileMap : public tbb::concurrent_hash_map<bfs::path, Mimfile, PathCompare> { };
  
ArtifactFiles::ArtifactFiles(bfs::path mountPoint, bfs::path storagePoint) :
  mimfiles(*new MimfileMap()),
  mountPoint(mountPoint),
  storagePoint(storagePoint) { }

ArtifactFiles::~ArtifactFiles() {
  delete &mimfiles;
}


void ArtifactFiles::mimfileUpdate(bfs::path file) {
  mimfileInDir(file.parent_path()).refresh();
}


Mimfile& ArtifactFiles::mimfileInDir(bfs::path dir) {
  MimfileMap::accessor acc;
  if (not mimfiles.find(acc, dir)) {
    mimfiles.insert(acc, dir);
	// TODO: The Mimfile constructor should not need the first argument.
    acc->second = Mimfile(storagePoint / dir / "mimfile", mountPoint, storagePoint, dir);
  }
  return acc->second;
}


int ArtifactFiles::rename(bfs::path oldname, bfs::path newname) {
  return mimfileInDir(oldname.parent_path()).rename(oldname, newname);
}

Filenames ArtifactFiles::ls(bfs::path directory) {
  return mimfileInDir(directory).files();
}
  
bool ArtifactFiles::exists(bfs::path file) {
  const Filenames& files = ls(file.parent_path());
  return std::find(files.begin(), files.end(), file.filename()) != files.end();
}
  
Artifact& ArtifactFiles::artifactNamed(bfs::path file) {
  return mimfileInDir(file.parent_path()).artifactNamed(file.filename());
}

}
