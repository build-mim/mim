#include "core/LoopbackFilesystem.hh"
#include "core/Mim.hh"
#include "core/Artifact.hh"

#include <sys/types.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <dirent.h>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <boost/filesystem/path.hpp>
#include <algorithm>


#define LOOPBACK_MAX_PATH_NAME 512
#define RETURN_ERRNO(x) (x) == 0 ? 0 : -errno
#define log(args...) printf("## EVENT: "); printf(args)

namespace bfs = boost::filesystem;


mim::ArtifactFiles& artifactFiles() {
  return mim::Mim::artifactFiles(fuse_get_context()->private_data);
}

mim::LeafFiles& leafFiles() {
  return mim::Mim::leafFiles(fuse_get_context()->private_data);
}

mim::BuildProcesses& buildProcesses() {
  return mim::Mim::buildProcesses(fuse_get_context()->private_data);
}

static void AbsPath(char *dest, const char *path) {
  bfs::path p = mim::Mim::abspath(fuse_get_context()->private_data, path);
  strncpy(dest, p.string().c_str(), LOOPBACK_MAX_PATH_NAME);
}


static int returnAttr(std::string src, char* dest, size_t size) {
  if (dest != 0 and src.size() <= size + 1)
    strcpy(dest, src.c_str());
  return src.size() + 1;
}

static int returnAttr(int src, char* dest, size_t size) {
  if (dest != 0 and sizeof(src) <= size)
    memcpy(dest, &src, sizeof(src));
  return sizeof(src);
}



static std::string incomingMessageQueueName() {
   char buffer[64];
   snprintf(buffer, 64, "mim-server-%d", getpid());
   return buffer;
}
static int returnMimPath(const char* path, char* dest, size_t size) {
  std::string queue = incomingMessageQueueName();
  size_t needed = strlen(path) + 1 + queue.size() + 1;
  if (dest != 0 and needed <= size) {
    strcpy(dest, queue.c_str());
    strcpy(dest + queue.size() + 1, path);
  }
  return needed;
}


class FuseInterface {
  mim::Mim& mim;
public:
  FuseInterface(mim::Mim& mim) : mim(mim) { }

  int getattr(const char* path, struct stat* statbuf) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    int error = RETURN_ERRNO(lstat(fullPath, statbuf));
    if (error != 0 and artifactFiles().exists(path))
      return artifactFiles().artifactNamed(path).stat(*statbuf);
    return error;
  }


  int readlink(const char* path, char* link, size_t size) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    int res = ::readlink(fullPath, link, size - 1);
    if(res == -1)
      return -errno;

    link[res] = '\0';
    return 0;
  }

  int mknod(const char* path, mode_t mode, dev_t dev) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);

    //handles creating FIFOs, regular files, etc...
    return RETURN_ERRNO(::mknod(fullPath, mode, dev));
  }

  int mkdir(const char* path, mode_t mode) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    return RETURN_ERRNO(::mkdir(fullPath, mode));
  }

  int unlink(const char *path) {
    if (artifactFiles().exists(path))
      artifactFiles().artifactNamed(path).makeNotUpdated();
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
  
    pid_t pid = buildProcesses().buildProcessThatStarted(fuse_get_context()->pid);
    if (pid != -1)
      ::close(::open(fullPath, O_CREAT)); // This is a unlink call by the build process, thus, we need to create the file if it does not exist.
  
    return RETURN_ERRNO(::unlink(fullPath));
  }

  int rmdir(const char *path) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    return RETURN_ERRNO(::rmdir(fullPath));
  }

  int symlink(const char *path, const char *link) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    return RETURN_ERRNO(::symlink(fullPath, link));
  }

  int rename(const char *path, const char *newpath) {
    if (artifactFiles().exists(newpath))
      artifactFiles().artifactNamed(newpath).makeNotUpdated();
    else
      leafFiles().modified(newpath);
    if (artifactFiles().exists(path))
      return artifactFiles().rename(path, newpath);

    char fullPath[LOOPBACK_MAX_PATH_NAME];
    char fullNewPath[LOOPBACK_MAX_PATH_NAME];  
    AbsPath(fullPath, path);
    AbsPath(fullNewPath, newpath);
    return RETURN_ERRNO(::rename(fullPath, fullNewPath));
  }

  int link(const char *path, const char *newpath) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    char fullNewPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    AbsPath(fullNewPath, newpath);
    return RETURN_ERRNO(::link(fullPath, fullNewPath));
  }

  int chmod(const char *path, mode_t mode) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    return RETURN_ERRNO(::chmod(fullPath, mode));
  }

  int chown(const char *path, uid_t uid, gid_t gid) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    return RETURN_ERRNO(::chown(fullPath, uid, gid));
  }

  int truncate(const char *path, off_t newSize) {
    if (not artifactFiles().exists(path))
      leafFiles().modified(path);

    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    pid_t pid = buildProcesses().buildProcessThatStarted(fuse_get_context()->pid);
    if (pid != -1)
      ::close(::open(fullPath, O_CREAT)); // This is a truncate call by the build process, thus, we need to create the file if it does not exist.
    return RETURN_ERRNO(::truncate(fullPath, newSize));
  }

  int utime(const char *path, struct utimbuf *ubuf) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    return RETURN_ERRNO(::utime(fullPath, ubuf));
  }

  int open(const char *path, struct fuse_file_info *fileInfo) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    fileInfo->fh = ::open(fullPath, fileInfo->flags);

    // TODO: This should be in the loopback_write function instead, because it's
    // not until something actually is written to the file that the artifacts needs
    // to be rebuilt.
    if ((fileInfo->flags & O_WRONLY or fileInfo->flags & O_RDWR) and not artifactFiles().exists(path))
      leafFiles().modified(path);

    return 0;
  }

  int read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fileInfo) {
    ssize_t bytesRead = ::pread(fileInfo->fh, buf, size, offset);
	  if (bytesRead < 0)
        return -errno;
    return bytesRead;
  }

  int write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fileInfo) {
    if (not artifactFiles().exists(path))
      leafFiles().modified(path);
    ssize_t bytesWritten = ::pwrite(fileInfo->fh, buf, size, offset);
    if (bytesWritten < 0)
      return -errno;
    return bytesWritten;
  }

  int statfs(const char *path, struct statvfs *statInfo) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    return RETURN_ERRNO(::statvfs(fullPath, statInfo));
  }

  int flush(const char *path, struct fuse_file_info *fileInfo) {
    ::fsync(fileInfo->fh);
    return 0;
  }

  int release(const char *path, struct fuse_file_info *fileInfo) {
    ::close(fileInfo->fh);
    if ((fileInfo->flags & O_WRONLY or fileInfo->flags & O_RDWR) and bfs::path(path).filename() == "mimfile")
      artifactFiles().mimfileUpdate(path);
    return 0;
  }

  int fsync(const char *path, int datasync, struct fuse_file_info *fi) {
    if (datasync) {
      //sync data only
      return RETURN_ERRNO(::fdatasync(fi->fh));
    } else {
      //sync data + file metadata
      return RETURN_ERRNO(::fsync(fi->fh));
    }
  }

  int setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    return RETURN_ERRNO(::lsetxattr(fullPath, name, value, size, flags));
  }

  int getxattr(const char *path, const char *name, char *value, size_t size) {
    if (artifactFiles().exists(path)) {
      if (std::string(name) == "user.mim_command")
        return returnAttr(artifactFiles().artifactNamed(path).command(), value, size);
      if (std::string(name) == "user.mim_stderr")
        return returnAttr(artifactFiles().artifactNamed(path).stderrFilename().string(), value, size);
      if (std::string(name) == "user.mim.path")
        return returnMimPath(path, value, size);
    }

    if (artifactFiles().exists(path) and std::string(name) == "user.mim_update") {
      if (size < sizeof(int))
        return sizeof(int);
      printf("### TODO: used.mim_update command not implemented!\n");
      int status = -1;
      return returnAttr(status, value, size);
    }


    char fullPath[LOOPBACK_MAX_PATH_NAME];
    AbsPath(fullPath, path);
    return RETURN_ERRNO(::getxattr(fullPath, name, value, size));
  }

};

static FuseInterface* fuseInterface = 0;
FuseInterface& interface() {
  if (not fuseInterface) {
    mim::Mim* mim = reinterpret_cast<mim::Mim*>(fuse_get_context()->private_data);
    fuseInterface = new FuseInterface(*mim);
  }
  return *fuseInterface;
}

static int loopback_getattr(const char* path, struct stat* statbuf) {
  log("getattr(%s)\n", path);
  return interface().getattr(path, statbuf);
}

static int loopback_readlink(const char *path, char *link, size_t size) {
  log("readlink(path=%s)\n", path, link, size);
  return interface().readlink(path, link, size);
}

static int loopback_mknod(const char *path, mode_t mode, dev_t dev) {
  log("mknod(path=%s, mode=%d)\n", path, mode);
  return interface().mknod(path, mode, dev);
}

static int loopback_mkdir(const char *path, mode_t mode) {
  log("mkdir(path=%s, mode=%d)\n", path, (int)mode);
  return interface().mkdir(path, mode);
}

static int loopback_unlink(const char *path) {
  log("unlink(path=%s\n)", path);
  return interface().unlink(path);
}

static int loopback_rmdir(const char *path) {
  log("rmdir(path=%s\n)", path);
  return interface().rmdir(path);
}

static int loopback_symlink(const char *path, const char *link) {
  log("symlink(path=%s, link=%s)\n", path, link);
  return interface().symlink(path, link);
}

static int loopback_rename(const char *path, const char *newpath) {
  log("rename(path=%s, newPath=%s)\n", path, newpath);
  return interface().rename(path, newpath);
}

static int loopback_link(const char *path, const char *newpath) {
  log("link(path=%s, newPath=%s)\n", path, newpath);
  return interface().link(path, newpath);
}

static int loopback_chmod(const char *path, mode_t mode) {
  log("chmod(path=%s, mode=%d)\n", path, mode);
  return interface().chmod(path, mode);
}

static int loopback_chown(const char *path, uid_t uid, gid_t gid) {
  log("chown(path=%s, uid=%d, gid=%d)\n", path, (int)uid, (int)gid);
  return interface().chown(path, uid, gid);
}

static int loopback_truncate(const char *path, off_t newSize) {
  log("truncate(path=%s, newSize=%d\n", path, (int)newSize);
  return interface().truncate(path, newSize);
}

static int loopback_utime(const char *path, struct utimbuf *ubuf) {
  log("utime(path=%s)\n", path);
  return interface().utime(path, ubuf);
}

static int loopback_open(const char *path, struct fuse_file_info *fileInfo) {
  log("open(path=%s)\n", path);
  return interface().open(path, fileInfo);
}

static int loopback_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fileInfo) {
  log("read(path=%s, size=%d, offset=%d)\n", path, (int)size, (int)offset);
  return interface().read(path, buf, size, offset, fileInfo);
}

static int loopback_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fileInfo) {
  log("write(path=%s, size=%d, offset=%d)\n", path, (int)size, (int)offset);
  return interface().write(path, buf, size, offset, fileInfo);
}

static int loopback_statfs(const char *path, struct statvfs *statInfo) {
  log("statfs(path=%s)\n", path);
  return interface().statfs(path, statInfo);
}

static int loopback_flush(const char *path, struct fuse_file_info *fileInfo) {
  log("flush(path=%s)\n", path);
  return interface().flush(path, fileInfo);
}

static int loopback_release(const char *path, struct fuse_file_info *fileInfo) {
  log("release(path=%s)\n", path);
  return interface().release(path, fileInfo);
}

static int loopback_fsync(const char *path, int datasync, struct fuse_file_info *fi) {
  log("fsync(path=%s, datasync=%d\n", path, datasync);
  return interface().fsync(path, datasync, fi);
}

static int loopback_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
  log("setxattr(path=%s, name=%s, value=%s, size=%d, flags=%d\n",
         path, name, value, (int)size, flags);
  return interface().setxattr(path, name, value, size, flags);
}

static int loopback_getxattr(const char *path, const char *name, char *value, size_t size) {
  log("getxattr(path=%s, name=%s, size=%d)\n", path, name, (int)size);
  return interface().getxattr(path, name, value, size);
}

static int loopback_listxattr(const char *path, char *list, size_t size) {
  log("listxattr(path=%s, size=%d)\n", path, (int)size);
  
  if (artifactFiles().exists(path)) {
    std::string attr = "user.mim_command";
    if (attr.size() + 1 < size) { // Need one more char to terminate the list of strings.
      list[attr.size() + 1] = 0; // To terminate the _list_ of strings. 
      strcpy(list, attr.c_str());
	}
    return attr.size() + 1; // Need one more null character to terminate the _list_ of string.
  }
  
  char fullPath[LOOPBACK_MAX_PATH_NAME];
  AbsPath(fullPath, path);
  return RETURN_ERRNO(llistxattr(fullPath, list, size));
}

static int loopback_removexattr(const char *path, const char *name) {
  log("removexattry(path=%s, name=%s)\n", path, name);
  char fullPath[LOOPBACK_MAX_PATH_NAME];
  AbsPath(fullPath, path);
  return RETURN_ERRNO(lremovexattr(fullPath, name));
}

static int loopback_opendir(const char *path, struct fuse_file_info *fileInfo) {
  log("opendir(path=%s)\n", path);
  char fullPath[LOOPBACK_MAX_PATH_NAME];
  AbsPath(fullPath, path);
  DIR *dir = opendir(fullPath);
  fileInfo->fh = (uint64_t)dir;
  return NULL == dir ? -errno : 0;
}

static int loopback_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fileInfo) {
  log("readdir(path=%s, offset=%d)\n", path, (int)offset);
  DIR *dir = (DIR*)fileInfo->fh;
  struct dirent *de = readdir(dir);
  if (NULL == de)
    return -errno;

  std::vector<boost::filesystem::path> names = artifactFiles().ls(path);
  do {
    if (std::find(names.begin(), names.end(), de->d_name) != names.end())
      continue; // Don't list built artifact twice.
    if (filler(buf, de->d_name, NULL, 0) != 0)
      return -ENOMEM;
  } while (NULL != (de = readdir(dir)));
  
  for (size_t i = 0; i < names.size(); i++) {
	if (filler(buf, names[i].string().c_str(), NULL, 0) != 0)
	  return -ENOMEM;
  }
  
  return 0;
}

static int loopback_releasedir(const char *path, struct fuse_file_info *fileInfo) {
  log("releasedir(path=%s)\n", path);
  closedir((DIR*)fileInfo->fh);
  return 0;
}

static int loopback_fsyncdir(const char *path, int datasync, struct fuse_file_info *fileInfo) {
  return 0;
}

static void* loopback_init(struct fuse_conn_info *conn) {
	return fuse_get_context()->private_data; // private_data points to the mim::Mim instance.
}

static void loopback_destroy(void* data) {
  delete reinterpret_cast<mim::Mim*>(data);
}

namespace mim {
  
LoopbackOperations::LoopbackOperations() {
  memset(this, sizeof(*this), 0);
  getattr = loopback_getattr;
  readlink = loopback_readlink;
  mknod = loopback_mknod;
  mkdir = loopback_mkdir;
  unlink = loopback_unlink;
  rmdir = loopback_rmdir;
  symlink = loopback_symlink;
  rename = loopback_rename;
  link = loopback_link;
  chmod = loopback_chmod;
  chown = loopback_chown;
  truncate = loopback_truncate;
  utime = loopback_utime;
  open = loopback_open;
  read = loopback_read;
  write = loopback_write;
  statfs = loopback_statfs;
  flush = loopback_flush;
  release = loopback_release;
  fsync = loopback_fsync;
  setxattr = loopback_setxattr;
  getxattr = loopback_getxattr;
  listxattr = loopback_listxattr;
  removexattr = loopback_removexattr;
  opendir = loopback_opendir;
  readdir = loopback_readdir;
  releasedir = loopback_releasedir;
  fsyncdir = loopback_fsyncdir;
  init = loopback_init;
  destroy = loopback_destroy;
}

}
