#ifndef MIM_ARTIFACT
#define MIM_ARTIFACT
#include <string>
#include <boost/filesystem/path.hpp>
#include <tbb/atomic.h>
#include <tbb/concurrent_hash_map.h>
#include <sys/stat.h>
#include <stdio.h>

namespace mim {

namespace bfs = boost::filesystem;
class Artifact;

struct ArtifactCompare {
  static size_t hash(Artifact* const& p) { return size_t(p); }
  static bool equal(Artifact* const& x, Artifact* const& y) { return x == y; }
};

class ArtifactSet : public tbb::concurrent_hash_map<Artifact*, char, ArtifactCompare> { };

// TODO: The needsUpdating function modifies 'dirty' in a non thread-safe way.
class Artifact {
  std::string cmd;
  bfs::path name;
  mutable tbb::atomic<bool> dirty;
  
  ArtifactSet dependencies;
  ArtifactSet users;
  
  struct ::stat attribs;

  void dependencyRenamed(const Artifact& dep, bfs::path oldname);
  
public:
  Artifact();

  //! \param filename the full file name, i.e., including path.
  //! \param command the shell command to execute to build the artifact.
  //! \param attribs the attributes to use before the artifact has been built.
  Artifact(bfs::path const& filename,
           std::string const& command,
           struct ::stat const& attribs);
  ~Artifact();
    
  Artifact& operator=(const Artifact& rhs);
    
  bfs::path filename() const { return name.filename(); }
  bfs::path fullFilename() const { return name; }

  std::string command() const {
    return cmd;
  }

  bfs::path stderrFilename() const {
    return "/tmp/mim" + name.string() + ".stderr";
  }

  bfs::path stdoutFilename() const {
    return "/tmp/mim" + name.string() + ".stdout";
  }


  void makeUpdated() { dirty = false; }
  void makeNotUpdated() {
    printf("%s needs to be updated!\n", filename().string().c_str());
    dirty = true;
  }
  bool needsUpdating() const;
  
  //! Define that this artifact depends on the provided artifact.
  void dependsOn(Artifact& artifact);
  
  int stat(struct ::stat& s);
  
  int rename(bfs::path newname);
};

  
}

#endif
