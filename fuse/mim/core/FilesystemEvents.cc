#include "include/mim.hh"

#include <fuse.h>

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <proc/readproc.h>
#include <sys/types.h>

#include "Mim.hh"
#include "Filenames.hh"
#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

namespace mim {

struct Message {
  Message(pid_t pid, struct read_event_args args, struct fuse_continuation cont) :
      pid(pid), args(args), cont(cont) { }
  pid_t pid;
  struct read_event_args args;
  struct fuse_continuation cont;
  
  ArtifactFiles& artifactFiles() { return mim::Mim::artifactFiles(args.user_data); }
  BuildProcesses& buildProcesses() { return mim::Mim::buildProcesses(args.user_data); }
};

static void* updateArtifactFile(void *ptr) {
  Message* m = (Message*)ptr;
  Artifact& artifact = m->artifactFiles().artifactNamed(m->args.path);
  bfs::create_directories(artifact.stderrFilename().parent_path());
  bfs::create_directories(artifact.stdoutFilename().parent_path());
  int stdout = open(artifact.stdoutFilename().string().c_str(), O_WRONLY|O_CREAT|O_TRUNC);
  int stderr = open(artifact.stderrFilename().string().c_str(), O_WRONLY|O_CREAT|O_TRUNC);
  int status = m->buildProcesses().executeProcessFor(artifact, m->pid, stdout, stderr);
  close(stdout);
  close(stderr);
  if (status == 0) {
    artifact.makeUpdated();
    printf("### Built %s successfully.\n", m->args.path);
  } else {
    printf("### Failed to build %s; status: %d\n", m->args.path, status);
  }
  fuse_invalidate_cache(m->args.req, m->args.ino);
  // Note: fuse error codes must be between 0 and -511.
  m->cont.func(m->cont.args, status == 0 ? 0 : -(status | MIM_ERROR_MASK));
  delete m;
  return NULL;
}

}

extern "C" {
  
  /**
   * This is a magic function name. If this function is defined, Fuse calls it when a file is opened instead
   * of calling the "open" function in the "fuse_operations" struct. The normal "open" function in the 
   * "fuse_operations" struct is called by invoking the "continuation" struct.
   */
  void file_read_event(struct read_event_args* args, struct fuse_continuation cont) {
    char* path = args->path;
    pid_t pid = fuse_req_ctx(args->req)->pid;
    
    pid_t starter = mim::Mim::buildProcesses(args->user_data).buildProcessThatStarted(pid);
    printf("#### Read event for '%s' ", path);

    if (starter > 0) { // Process started by a mim build process
      mim::Artifact& requester = mim::Mim::buildProcesses(args->user_data).artifactBeingBuiltBy(starter);
      if (requester.fullFilename() == path) {
        printf("is made by its build process, thus, will update this file again.\n");
        cont.func(cont.args, 0);
        return;
      }
      printf("(needed by '%s')", requester.fullFilename().string().c_str());
    } else {
      printf("(requested by user)");
    }
    
  
    if (mim::Mim::artifactFiles(args->user_data).exists(path)) {
      mim::Artifact& toBuild = mim::Mim::artifactFiles(args->user_data).artifactNamed(path);
      
      if (starter > 0) {
        mim::Artifact& requester = mim::Mim::buildProcesses(args->user_data).artifactBeingBuiltBy(starter);
        requester.dependsOn(toBuild);
      }

      if (not toBuild.needsUpdating()) {
        printf(", which is up to date. Will not update it.\n");
        cont.func(cont.args, 0);
        return;
      } else {
        printf(", which needs update.\n");
        pthread_t thread1;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED); // Will never do join on the thread.
        pthread_create(&thread1, &attr, mim::updateArtifactFile,
                       new mim::Message(fuse_req_ctx(args->req)->pid, *args, cont));
        pthread_attr_destroy(&attr);
      }
    } else {
      printf(", which is a leaf file.\n");
      if (starter > 0) {
        mim::Artifact& requester = mim::Mim::buildProcesses(args->user_data).artifactBeingBuiltBy(starter);
        mim::Mim::leafFiles(args->user_data).defineDependency(requester, path);
      }
      cont.func(cont.args, 0);
    }    
  }
}

