#ifndef MIM_SERIALIZER_HH
#define MIM_SERIALIZER_HH

#include <stdio.h>
#include <inttypes.h>
#include <assert.h>
#include <string.h>
#include <string>
#include <boost/preprocessor/repetition/repeat_from_to.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>

//! The max number of messages. Limit is 256. The messages should have IDs in the
//! range [0, MAX_MESSAGES]
#ifndef MAX_MESSAGES
#define MAX_MESSAGES 16
#endif




namespace serializer {

//! Serializes types into a array of chars.
class SerializeBuffer {
  char* _dest;
  size_t _size;
  size_t _used;

  template<typename TYPE>
  void serializeInt(TYPE i) {
    memcpy(_dest + _used, &i, sizeof(TYPE));
    _used += sizeof(TYPE);
  }

public:
 
  SerializeBuffer(char* dest, size_t size) :
    _dest(dest), _size(size), _used(0) { }

  void handle(char i) { serializeInt<char>(i); }
  void handle(uint8_t i) { serializeInt<uint8_t>(i); }
  void handle(int8_t i) { serializeInt<int8_t>(i); }
  void handle(int16_t i) { serializeInt<int16_t>(i); }
  void handle(uint16_t i) { serializeInt<uint16_t>(i); }
  void handle(int32_t i) { serializeInt<int32_t>(i); }
  void handle(uint32_t i) { serializeInt<uint32_t>(i); }
  void handle(int64_t i) { serializeInt<int64_t>(i); }
  void handle(uint64_t i) { serializeInt<uint64_t>(i); }

  //! Serialize an array of any type.
  template<typename ELEMENT_TYPE>
  void handle(const ELEMENT_TYPE* buffer, size_t elementsInBuffer) {
    handle(uint16_t(elementsInBuffer));
    for (size_t i = 0; i < elementsInBuffer; i++)
      handle(buffer[i]);
  }

  void handle(std::string s) {
    handle(s.c_str(), s.size());
  }

  size_t used() const {
    return _used;
  }

};


//! Unserializes types from a array of chars.
template<typename DATA_PROVIDER>
class UnserializeBuffer {
  DATA_PROVIDER& _provider;
  size_t _size;
  size_t _used;

  template<typename TYPE>
  TYPE unserializeInt() {
    TYPE i;
    _provider(&i, sizeof(TYPE));
    _used += sizeof(TYPE);
    return i;
  }

public:
  UnserializeBuffer(DATA_PROVIDER& provider, size_t size) :
    _provider(provider), _size(size), _used(0) { }

  void handle(char& i) { i = unserializeInt<char>(); }
  void handle(int8_t& i) { i = unserializeInt<int8_t>(); }
  void handle(uint8_t& i) { i = unserializeInt<uint8_t>(); }
  void handle(int16_t& i) { i = unserializeInt<int16_t>(); }
  void handle(uint16_t& i) { i = unserializeInt<uint16_t>(); }
  void handle(int32_t& i) { i = unserializeInt<int32_t>(); }
  void handle(uint32_t& i) { i = unserializeInt<uint32_t>(); }
  void handle(int64_t& i) { i = unserializeInt<int64_t>(); }
  void handle(uint64_t& i) { i = unserializeInt<uint64_t>(); }

  //! Unserialize an array of any type.
  template<typename ELEMENT_TYPE>
  void handle(ELEMENT_TYPE* buffer, size_t _) {
    uint16_t elementsInBuffer;
    handle(elementsInBuffer);
    for (size_t i = 0; i < elementsInBuffer; i++)
      handle(buffer[i]);
  }

  void handle(std::string& s) {
    // Read the size of the string without advancing the read-pointer.
    uint16_t size;
    handle(size);
    s.resize(size);
    _provider(&s[0], size);
  }

  size_t used() const {
    return _used;
  }
};


// Represents an unused ID in the message id range.
template<int ID>
struct Unused {
  template<typename T>
  void dispatch(T) {
    printf("Unhandled message of type %d\n", ID);
  }
};
template<class BUFFER, int ID>
void traverse(BUFFER& dest, Unused<ID>& msg) {
}

//! The 'traverse' function traverses the data structure and lets the BUFFER
//! handle each element of the data structure that should be (un)serialized.
//! The traverse function is used for both serializing and unserializing.
//! Each message that can be serialize must implement a traverse function that
//! serializes each field of the message.
template<class BUFFER>
void traverse(BUFFER& dest, std::string& msg) {
  dest.handle(msg);
}

template<class BUFFER>
void traverse(BUFFER& dest, char& msg) {
  dest.handle(msg);
}

template<class BUFFER>
void traverse(BUFFER& dest, int64_t& msg) {
  dest.handle(msg);
}

template<class BUFFER>
void traverse(BUFFER& dest, uint64_t& msg) {
  dest.handle(msg);
}



#define DECL(z, n, text) BOOST_PP_COMMA_IF(n) typename A##n = Unused<n>
template<BOOST_PP_REPEAT_FROM_TO(0, MAX_MESSAGES, DECL, dummy)>
#undef DECL
class Serializer {
  template<typename T, typename DATA_PROVIDER>
  static T _decode(DATA_PROVIDER& provider) {
    UnserializeBuffer<DATA_PROVIDER> buffer(provider, 100);
    T t;
    traverse(buffer, t);
    return t;
  }

  template<class A>
  static int _encode(A& obj, char* outbuff, uint8_t id) {
    outbuff[0] = id;
    SerializeBuffer sb(outbuff + 1, 100);
    traverse(sb, obj);
    return sb.used() + 1;
  }

#define DECL(z, n, text) template<typename HANDLER> static void _handle(HANDLER&, Unused<n> const&) { }
  BOOST_PP_REPEAT_FROM_TO(0, MAX_MESSAGES, DECL, dummy)
#undef DECL

  template<typename HANDLER, typename MSG>
  static void _handle(HANDLER& handler, MSG const& msg) {
    handler.handle(msg);
  }

public:

  template<typename HANDLER, typename DATA_PROVIDER>
  static void decode(HANDLER& handler, DATA_PROVIDER& provider) {
    uint8_t type;
    provider(&type, sizeof(type));
    switch(type) {
#define DECL(z, n, text) case n: _handle(handler, _decode< A##n, DATA_PROVIDER>(provider)); return;
      BOOST_PP_REPEAT_FROM_TO(0, MAX_MESSAGES, DECL, dummy)
#undef DECL
      default: printf("Unkown type %d\n", type);
    }
  }

#define DECL(z, n, text) static size_t encode(A##n& msg, char* outbuff) { return _encode(msg, outbuff, n); }
  BOOST_PP_REPEAT_FROM_TO(0, MAX_MESSAGES, DECL, dummy)
#undef DECL
};

}

#endif // MIM_SERIALIZER_HH
