#include "LoopbackFilesystem.hh"
#include "CommandInterface.hh"
#include "Mim.hh"
#include <stdio.h>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <string>
#include <unistd.h>
char *getcwd(char *buf, size_t size); 

static mim::LoopbackOperations loopbackOperations;

using boost::filesystem::path;

path complete_path(const char* cwd, const char* dir) {
  return boost::filesystem::complete(dir);
}

int main_start(int argc, char* argv[]) {
  path mountPoint = complete_path(argv[0], argv[argc - 1]);
  path storagePoint = complete_path(argv[0], argv[argc - 2]);
  mim::Mim* mim = new mim::Mim(mountPoint, storagePoint);

  printf("mounting file system... (%s -> %s)\n", storagePoint.string().c_str(), mountPoint.string().c_str());
  int i, fuse_stat;
  for (i = 1; i < argc && (argv[i][0] == '-'); i++) {
    if (i == argc)
      return (-1);
  }

  for(; i < argc; i++)
    argv[i] = argv[i+1];

  argc--;
  mim::startCommandInterface(*mim);

  // LoopbackOperations takes owenership of the mim::Mim instance.
  fuse_stat = fuse_main(argc, argv, &loopbackOperations, mim);
  printf("fuse_main returned %d\n", fuse_stat);

  return fuse_stat;
}


int main_help(int argc, char *argv[]) {
  printf("  mim help\n");
  printf("    print this help text.\n");
  printf("\n");
  printf("  mim storage-mount-point artifact-mount-point, where\n");
  printf("    storage-mount-point\t\tthe path on disk where physical files are stored.\n");
  printf("    artifact-mount-point\tthe path on disk the virtual directory structure will be mounted.\n");
  printf("\n");
  printf("  Artifacts attributes:\n");
  printf("    mim_command           the full command that builds the artifact.\n");
  printf("    mim_symbolic_command  the command entry in the Mimfile for the artifact.\n");
  printf("\n");
  printf("  FUSE options:\n");
  printf("    -d                    enable debug output (implies -f).\n");
  printf("    -f                    foreground operation.\n");
  printf("    -s                    disable multi-threaded operation.\n");
  return 0;
}


int main(int argc, char *argv[]) {
  if ((argc == 2 and std::string("help") == argv[1]) or (argc <= 1))
    return main_help(argc, argv);
  else
    return main_start(argc, argv);
}
