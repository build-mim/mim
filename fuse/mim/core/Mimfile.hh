#ifndef MIM_MIMFILE
#define MIM_MIMFILE

#include "Filenames.hh"
#include "Artifact.hh"
#include <boost/filesystem/path.hpp>
#include <string>
#include <map>

struct lua_State;

namespace mim {

namespace bfs = boost::filesystem;

class Mimfile { 
  bfs::path mimfileName;
  bfs::path mountPoint;
  bfs::path storagePoint;
  bfs::path virtualDir;
  
  class ArtifactMap : public std::map<bfs::path, Artifact> { };
  
  Filenames artifactFiles;
  ArtifactMap artifacts;

  void openMimfile();
  std::string symbolicCmdToRealCmd(std::string cmd, bfs::path filename);

public:

  Mimfile() { }
  
  /**
   * physicalFilename the path to the mimfile (the underlying directory where it's stored)
   * virtualDir the Mim-directory directory where the mimfile is stored.
   */
  Mimfile(bfs::path physicalFilename, bfs::path mountPoint,
          bfs::path storagePoint, bfs::path virtualDir);

  //! Get the list of files that is defined by this mimfile.  
  Filenames files();

  //! Get the artifact with name \p filename (just the file name, no directories).
  Artifact& artifactNamed(bfs::path filename);
  
  //! Reread the mimfile from disk and update all related data structures.
  void refresh();
  
  //! Rename an artifact file.
  //! \returns see stdio.h rename function.
  int rename(bfs::path oldname, bfs::path newname);

  //! Add a new artifact to this directory.
  //! \param filename the name of the artifact (excluding path).
  //! \param command the shell command to execute for building the artifact.
  //! \param attribs the attributes of the artifact which will be used when the
  //! artifact hasn't be built yet.
  void newArtifact(bfs::path const& filename,
                   std::string const& command,
                   struct ::stat const& attribs);
};
  
  
}


#endif
