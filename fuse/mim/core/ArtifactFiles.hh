#ifndef MIM_ARTIFACT_FILES
#define MIM_ARTIFACT_FILES

#include <stdio.h>
#include <boost/filesystem/path.hpp>
#include <inttypes.h>
#include <sys/types.h>
#include <boost/filesystem/path.hpp>


#include "Mimfile.hh"
#include "Filenames.hh"

namespace mim {
  
namespace bfs = boost::filesystem;

class MimfileMap;  

/**
 * Represents all the artifact files in the file system. Note:
 *  - There should be only one instance of this class in the application.
 *  - This class is thread-safe.
 */
class ArtifactFiles {
  MimfileMap& mimfiles;
  bfs::path mountPoint;
  bfs::path storagePoint;
  
  Mimfile& mimfileInDir(bfs::path dir);

  //! Don't copy the object.
  ArtifactFiles(const ArtifactFiles&);
  ArtifactFiles& operator=(const ArtifactFiles&);

public:

  ArtifactFiles(bfs::path mountPoint, bfs::path storagePoint);
  ~ArtifactFiles();
  
  //! Notify that a mimfile has been update.
  void mimfileUpdate(bfs::path file);
  
  //! Get the set of artifact files in the given directory.
  Filenames ls(bfs::path directory);

  //! Check if there is an artifact file called 'name'
  bool exists(bfs::path file);
  
  int rename(bfs::path oldname, bfs::path newname);
  
  //! Get the artifact file with the name 'file' (relative to the mount point).
  Artifact& artifactNamed(bfs::path file);
};
  
  
}


#endif
