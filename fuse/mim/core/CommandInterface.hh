#ifndef MIM_COMMAND_INTERFACES
#define MIM_COMMAND_INTERFACES

#include "core/Serializer.hh"
#include <string>

namespace mim {

namespace messages { 

template<class BUFFER>
void traverse(BUFFER& dest, pid_t& msg) {
  dest.handle(msg);
}

//! Get the command used for building the artifact \p filename.
struct ReqArtifactInfo {
  std::string queue;
  std::string path;
};
template<class BUFFER>
void traverse(BUFFER& dest, messages::ReqArtifactInfo& msg) {
  traverse(dest, msg.queue);
  traverse(dest, msg.path);
}


struct RspArtifactInfo {
  std::string command;
  std::string stdoutFile;
  std::string stderrFile;
};

template<class BUFFER>
void traverse(BUFFER& dest, messages::RspArtifactInfo& msg) {
  traverse(dest, msg.command);
  traverse(dest, msg.stdoutFile);
  traverse(dest, msg.stderrFile);
}

struct RejArtifactInfo {
  std::string path;
};
template<class BUFFER>
void traverse(BUFFER& dest, messages::RejArtifactInfo& msg) {
  traverse(dest, msg.path);
}






}

//! The serializer that is used for sending messages from the server to the
//! client.
class ServerToClient : public serializer::Serializer< messages::RspArtifactInfo,
                                                      messages::RejArtifactInfo
                                                    > { };


//! The serializer that is used for sending messages from the client to the
//! server.
class ClientToServer : public serializer::Serializer< messages::ReqArtifactInfo
                                                    > { };


class Mim;
void startCommandInterface(Mim& mim);  




}

#endif
