#include "catch/catch.hpp"
#include "core/Artifact.hh"

using mim::Artifact;
TEST_CASE("Artifact/stat", "Artifact.stat uses provided attributes.") {
  struct ::stat initAttrs;
  initAttrs.st_ctime = 1;
  initAttrs.st_mtime = 2;
  initAttrs.st_nlink = 3;
  initAttrs.st_mode = 4;
  initAttrs.st_size = 5;
  initAttrs.st_gid = 6;
  initAttrs.st_uid = 7;
  initAttrs.st_atime = 8;
  Artifact artifact("path/filename", "command", initAttrs);

  REQUIRE("command" == artifact.command());
  REQUIRE("filename" == artifact.filename());
  REQUIRE("path/filename" == artifact.fullFilename());
  struct ::stat attrs;
  REQUIRE(0 == artifact.stat(attrs));
  REQUIRE(attrs.st_ctime == 1);
  REQUIRE(attrs.st_mtime == 2);
  REQUIRE(attrs.st_nlink == 3);
  REQUIRE(attrs.st_mode == 4);
  REQUIRE(attrs.st_size == 5);
  REQUIRE(attrs.st_gid == 6);
  REQUIRE(attrs.st_uid == 7);
  REQUIRE(attrs.st_atime == 8);
}
