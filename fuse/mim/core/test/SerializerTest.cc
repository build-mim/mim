#include "catch/catch.hpp"
#include "core/Serializer.hh"

struct Message0 { };
template<class BUFFER>
void traverse(BUFFER& dest, Message0& msg) { }

struct Message1 { char field0; char field1; };
template<class BUFFER>
void traverse(BUFFER& dest, Message1& msg) {
  traverse(dest, msg.field0);
  traverse(dest, msg.field1);
}

struct Message2 { std::string field; };
template<class BUFFER>
void traverse(BUFFER& dest, Message2& msg) {
  traverse(dest, msg.field);
}


struct Message3 { Message0 m0; Message1 m1; Message2 m2; };
template<class BUFFER>
void traverse(BUFFER& dest, Message3& msg) {
  traverse(dest, msg.m0);
  traverse(dest, msg.m1);
  traverse(dest, msg.m2);
}


class TestSerializer : public serializer::Serializer<Message0, Message1, Message2, Message3> { };

TEST_CASE("Serializer/encode", "Encode messages.") {
  Message0 m0 = { };
  char buffer[100];

  size_t used0 = TestSerializer::encode(m0, buffer);
  REQUIRE(used0 == 1);
  REQUIRE(buffer[0] == 0);

  Message1 m1 = { 'A', 'b' };
  size_t used1 = TestSerializer::encode(m1, buffer);
  REQUIRE(used1 == 3);
  REQUIRE(buffer[0] == 1);
  REQUIRE(buffer[1] == 'A');
  REQUIRE(buffer[2] == 'b');

  std::string str = "this";
  Message2 m2 = { str };
  size_t used2 = TestSerializer::encode(m2, buffer);
  REQUIRE(used2 == 1 + 2 + str.size());
  REQUIRE(buffer[0] == 2);
  REQUIRE(int(buffer[1]) == 4);
  REQUIRE(int(buffer[2]) == 0);
  REQUIRE(buffer[3] == 't');
  REQUIRE(buffer[4] == 'h');
  REQUIRE(buffer[5] == 'i');
  REQUIRE(buffer[6] == 's');

  Message3 m3 = { m0, m1, m2 };
  size_t used3 = TestSerializer::encode(m3, buffer);
  REQUIRE(used3 == 1 + used0 - 1 + used1 - 1 + used2 - 1);
}


class BufferReader {
  char* data;
public:
  BufferReader(char* data) : data(data) { }

  void operator()(void* dest, size_t size) {
    memcpy(dest, data, size);
    data += size;
  }
};

struct Handler {
  bool m0;
  Message1 m1;
  Message2 m2;
  Message3 m3;
  Handler() : m0(false) { }
  void handle(Message0 const& m) { m0 = true; }
  void handle(Message1 const& m) { m1 = m; }
  void handle(Message2 const& m) { m2 = m; }
  void handle(Message3 const& m) { m3 = m; }
};

TEST_CASE("Serializer/decode/empty", "Decode empty message.") {
  char buffer[] = { 0 };
  BufferReader reader(buffer);
  Handler handler;

  TestSerializer::decode(handler, reader);
  REQUIRE(handler.m0);
}


TEST_CASE("Serializer/decode/two_char", "Decode message with two char fields.") {
  char buffer[] = { 1, 'a', 'B' };
  BufferReader reader(buffer);
  Handler handler;

  TestSerializer::decode(handler, reader);
  REQUIRE('a' == handler.m1.field0);
  REQUIRE('B' == handler.m1.field1);
}

TEST_CASE("Serializer/decode/std_string", "Decode message with std::string.") {
  char buffer[] = { 2, 3, 0, 'a', 'b', 'c' };
  BufferReader reader(buffer);
  Handler handler;

  TestSerializer::decode(handler, reader);
  REQUIRE(3 == handler.m2.field.size());
  REQUIRE("abc" == handler.m2.field);
}


TEST_CASE("Serializer/decode/nested", "Decode message with nested messages.") {
  Message0 m0;
  Message1 m1 = { '1', '2' };
  Message2 m2 = { "a string" };
  Message3 m3 = { m0, m1, m2 };
  char buffer[100];
  TestSerializer::encode(m3, buffer);
  
  Handler handler;
  BufferReader reader(buffer);
  TestSerializer::decode(handler, reader);

  REQUIRE('1' == handler.m3.m1.field0);
  REQUIRE('2' == handler.m3.m1.field1);
  REQUIRE("a string" == handler.m3.m2.field);
}

