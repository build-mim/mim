#include "core/Artifact.hh"

#include <boost/algorithm/string/replace.hpp>
#include <stdio.h>
#include <stdlib.h>


namespace mim {

static const uint64_t RANDOM = rand();

Artifact::Artifact() { }

Artifact::Artifact(bfs::path const& filename,
                   std::string const& command,
                   struct ::stat const& attribs) : cmd(command),
                                                   name(filename),
                                                   attribs(attribs) {
  dirty = true;
}
  
  
Artifact::~Artifact() {
  for (ArtifactSet::iterator it = users.begin(); it != users.end(); ++it) {
    printf("%s is referring to deleted artifact %s. Your build is broken.\n", it->first->name.string().c_str(), name.string().c_str());
    it->first->makeNotUpdated();
    bool wasDeleted = it->first->dependencies.erase(this);
    if (not wasDeleted)
      printf("wasDeleted == true\n");
  }
}
  
Artifact& Artifact::operator=(const Artifact& rhs) {
  assert (name.empty() or name == rhs.name);
  name = rhs.name;
  attribs = rhs.attribs;
  if (rhs.cmd != cmd) {
    dependencies.clear();
    cmd = rhs.cmd;
    dirty = true;
  }
  return *this;
}
  
bool Artifact::needsUpdating() const {
  if (dirty)
    return true;
  for (ArtifactSet::const_iterator it = dependencies.begin(); it != dependencies.end(); ++it) {
    if (it->first->needsUpdating()) {
      dirty = true;
      return true;
    }
  }
  return false;
}

void Artifact::dependsOn(Artifact& artifact) {
  ArtifactSet::accessor a;
  dependencies.insert(a, &artifact);
  artifact.users.insert(a, this);
}

int Artifact::stat(struct ::stat& s) {
  s = attribs;
  return 0;
}

void Artifact::dependencyRenamed(const Artifact& dep, bfs::path oldname) {
  boost::algorithm::replace_all(cmd, oldname.string(), dep.filename().string());
  dirty = true;
}

int Artifact::rename(bfs::path newname) {
  bfs::path oldname = name.filename();
  name = newname;
  for (ArtifactSet::iterator it = users.begin(); it != users.end(); ++it)
    it->first->dependencyRenamed(*this, oldname);
  return 0;
}

  
}
