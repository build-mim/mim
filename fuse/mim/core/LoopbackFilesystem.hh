#ifndef MIM_LOOPBACK_FILESYSTEM
#define MIM_LOOPBACK_FILESYSTEM

#define FUSE_USE_VERSION 26
#include <fuse.h>

namespace mim {

struct LoopbackOperations : fuse_operations {
  LoopbackOperations();
};

}

#endif