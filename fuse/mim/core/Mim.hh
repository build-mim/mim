#ifndef MIM_MIM
#define MIM_MIM

#include "ArtifactFiles.hh"
#include "BuildProcesses.hh"
#include "LeafFiles.hh"
#include <boost/filesystem/path.hpp>

namespace mim {
  
/**
 * The main class of Mim.
 */ 
class Mim {
  ArtifactFiles _files;
  BuildProcesses _processes;
  LeafFiles _leaves;
  boost::filesystem::path _storagePoint;
  boost::filesystem::path _mountPoint;
public:

  Mim(boost::filesystem::path mountPoint,
      boost::filesystem::path storagePoint) :
    _files(mountPoint, storagePoint),
	_storagePoint(storagePoint),
    _mountPoint(mountPoint) { }

  
  ArtifactFiles& artifactFiles() {
    return _files;
  }

  static ArtifactFiles& artifactFiles(void* mim) {
    return reinterpret_cast<Mim*>(mim)->_files;
  }
  
  BuildProcesses& buildProcesses() {
    return _processes;
  }

  static BuildProcesses& buildProcesses(void* mim) {
    return reinterpret_cast<Mim*>(mim)->_processes;
  }

  LeafFiles& leafFiles() {
    return _leaves;
  }
  
  static LeafFiles& leafFiles(void* mim) {
    return reinterpret_cast<Mim*>(mim)->_leaves;
  }

  boost::filesystem::path abspath(std::string const& path) {
	return _storagePoint / path;
  }

  static boost::filesystem::path abspath(void* mim, const char *path) {
	return reinterpret_cast<Mim*>(mim)->_storagePoint / path;
  }

  boost::filesystem::path storagePoint() const {
    return _storagePoint;
  }

  boost::filesystem::path mountPoint() const {
    return _mountPoint;
  }


};

}

#endif
