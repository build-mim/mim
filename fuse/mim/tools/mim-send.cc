#include "include/mim2.hh"

#include <iostream>

namespace bi = boost::interprocess;

int main(int argc, const char** argv) {

  if (argc < 2) {
    printf("missing required argument: filename\n");
    return -1;
  }
  std::string filename = argv[1];

  mim::FileInfo info = mim::fileinfo(filename);
  if (info.queue.empty()) {
     printf("Not a mim file: %s\n", filename.c_str());
     return 1;
  }
   
  try {
	 printf("Opening channel to mim: %s\n", info.queue.c_str());
	 printf("Mim path: %s\n", info.path.c_str());
     mim::Mim api(mim::messageQueueName(info.queue), info.queue);
     std::string command = api.command(info.path);
     printf("%s\n", command.c_str());
  } catch (bi::interprocess_exception &ex) {
     std::cout << "error: " << ex.what() << std::endl;
     return 1;
  }
  return 0;
}

