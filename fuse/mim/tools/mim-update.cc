#include "include/mim.hh"
#include <stdio.h>


int update(const char* filename) {
  int buildStatus;
  int result = mim::update(filename, buildStatus);
  if (result == mim::OK) {
    if (buildStatus == 0)
	    printf("Built %s succefully.\n", filename);
	else
		printf("Failed to build %s; exit status: %d\n", filename, buildStatus);
  } else if (result == mim::NOT_MIMFILE)
    printf("%s is not a mim file.\n", filename);
  else
    printf("Failed to build %s.\n", filename);
  return result;
}


int main(int argc, char *argv[]) {
  if (argc == 2)
    return update(argv[1]);
  else
    printf("Expected: filename.\n");
}
