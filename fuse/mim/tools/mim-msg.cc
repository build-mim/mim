#include "include/mim.hh"
#include <stdio.h>
#include <iostream>
#include <error.h>

int printStderr(const char* filename) {
  std::string stderr;
  int result = mim::stderr(filename, stderr);
  if (result == mim::OK)
    std::cout << stderr;
  else if (result == mim::NOT_MIMFILE)
    error(1, 2, "%s is not a mim file.\n", filename);
  else
    error(2, 3, "Failed retrieve name of stderr file for %s.\n", filename);
  return result;
}


int main(int argc, char *argv[]) {
  if (argc == 2 or argc == 3)
    return printStderr(argv[1]);
  else
    printf("Expected: filename.\n");
}
