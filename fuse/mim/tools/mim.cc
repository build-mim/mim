#include "include/mim.hh"
#include "include/mim2.hh"
#include "core/CommandInterface.hh"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <fstream>
#include <sstream>

namespace mim {


template<typename T>
struct Attr {
  Attr(T attr) : attr(attr), valid(true) { }
  Attr() : valid(false) { }
  T attr;
  bool valid;
};

template<typename T>
T allocate(int size);

template<>
std::string allocate<std::string>(int size) {
  return std::string(size, '\0');  
}

template<>
int allocate<int>(int size) {
  assert(size >= sizeof(int));
  return 0;  
}



int* dataArea(int& n) {
	return &n;
}

char* dataArea(std::string& s) {
  return &s[0];
}




template<typename T>
int readattr(const char* filename, std::string mimFunction, T& output) {
  std::string attrib = "user.mim_" + mimFunction;
  ssize_t needed = getxattr(filename, attrib.c_str(), 0, 0);
  if (needed > 0) {
    T result = allocate<T>(needed);
    if (getxattr(filename, attrib.c_str(), dataArea(result), needed)) {
      output = result;
      return OK;
	}
  }
  return NOT_MIMFILE;
}


int update(const char* filename, int& result) {
  return readattr<int>(filename, "update", result);
}

int command(const char* filename, std::string& result, bool symbolic) {
  static const char* ATTR[] = { "command",
                                "symbolic_command" };
  return readattr<std::string>(filename, ATTR[symbolic], result);
}



void writeFileTo(std::string filename, std::string& output) {
  std::ifstream fin(filename.c_str());
  std::string lineFromFile;
  std::stringstream builder;

  while (getline(fin, lineFromFile))
    builder << lineFromFile << std::endl;
  output = builder.str();
}

int stderr(const char* filename, std::string& output) {
  std::string stderrFilename;
  int result = readattr<std::string>(filename, "stderr", stderrFilename);
  if (result != OK)
	return result;

  writeFileTo(stderrFilename, output);
  return result;
}





// mim2.hh


class MessageQueueReader {
  char const* buffer;
  size_t size;
public:
  MessageQueueReader(char const* buffer, size_t size) :
    buffer(buffer), size(size) { }

  void operator()(void* dest, size_t size) {
    memcpy(dest, buffer, size);
    buffer += size;
  }
};


//! Get a string that is per-process-unique.
std::string messageQueueName(std::string const& mimQ) {
  std::stringstream ss;
  ss << mimQ << "-client-" << getpid();
  return ss.str();
}

static const size_t MAX_MESSAGE_SIZE = 1024;


class ArtifactInfoHandler {
public:
  bool response;
  bool reject;
  messages::RspArtifactInfo msg;
  ArtifactInfoHandler() : response(false), reject(false) { }
  void handle(mim::messages::RspArtifactInfo const& m) {
    msg = m;
  }
  void handle(mim::messages::RejArtifactInfo const& m) {
  }
};


template<typename MSG, typename HANDLER>
void request(bi::message_queue& requestQ,
             bi::message_queue& responseQ,
             MSG message,
             HANDLER& handler) {
  char buffer[MAX_MESSAGE_SIZE];
  size_t used = ClientToServer::encode(message, buffer);
  requestQ.send(buffer, used, 0);

  size_t receivedSize;
  unsigned int priority;
  responseQ.receive(buffer, MAX_MESSAGE_SIZE, receivedSize, priority);

  MessageQueueReader reader(buffer, receivedSize);
  ServerToClient::decode(handler, reader);
}

Mim::Mim(std::string const& clientQueueName, std::string const& mimQueueName) :
       clientQueueName(clientQueueName),
       responseQ(bi::create_only,
                 clientQueueName.c_str(),
                 1,
                 MAX_MESSAGE_SIZE),
       requestQ(bi::open_only,
                mimQueueName.c_str()) { }


std::string Mim::command(std::string const& filename) {
  mim::messages::ReqArtifactInfo message = { clientQueueName,
                                             filename };
  ArtifactInfoHandler handler;
  request(requestQ, responseQ, message, handler);
  return handler.msg.command;
}

Mim::~Mim() {
   bi::message_queue::remove(clientQueueName.c_str());
}


FileInfo fileinfo(std::string const& filename) {
  static const char* ATTRIB = "user.mim.path";
  size_t available = filename.size() + 64;
  char buffer[available];
  if (0 < getxattr(filename.c_str(), ATTRIB, buffer, available)) {
    FileInfo info;
    info.queue = buffer;
    char* path = buffer + info.queue.size() + 1;
    info.path = path;
    return info;
  }
  return FileInfo();
}





}
