#include "include/mim.hh"
#include <stdio.h>
#include <string>


int command(const char* filename, bool symbolic) {
  std::string command;
  int result = mim::command(filename, command, symbolic);
  if (result == mim::OK)
    printf("%s\n", command.c_str());
  else if (result == mim::NOT_MIMFILE)
    printf("%s is not a mim file.\n", filename);
  else
    printf("Failed retrieve command for %s.\n", filename);
  return result;
}


int main(int argc, char *argv[]) {
  if (argc == 2 or argc == 3)
    return command(argv[1], (argc == 3 and std::string(argv[2]) == "-s"));
  else
    printf("Expected: filename.\n");
}
