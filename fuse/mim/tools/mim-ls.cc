#include "include/mim.hh"
#include <iostream>
#include <algorithm>
#include <string>
#include <iterator>
#include <stdio.h>
#include <boost/filesystem.hpp>

using namespace boost::filesystem;



struct pathname_of {
  std::string operator()(const directory_entry& p) const {
    std::string command;
    int result = mim::command(p.path().string().c_str(), command, true);
    if (result == mim::OK)
      return p.path().filename() + "\t" + command.c_str();
    return p.path().filename();
  }
};

int ls(const char* name)
{
  if (is_directory(path(name))) {
    transform(directory_iterator(name), directory_iterator(),
              std::ostream_iterator<std::string>(std::cout, "\n"),
              pathname_of());
  } else {
	printf("Only directories are supported so far.\n");
  }
  return 0;
}


int main(int argc, char *argv[]) {
  if (argc == 2)
    return ls(argv[1]);
  else
    printf("Expected: directory name.\n");
}
