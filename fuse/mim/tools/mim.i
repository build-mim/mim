%module mim

%include "std_string.i"

%{
#include "include/mim2.hh"
using namespace mim;
%}

std::string messageQueueName(std::string const& mimQ);

class Mim {
public:
  Mim(std::string const& apiQueueName, std::string const& mimQueueName);
  ~Mim();
  std::string command(std::string const& filename);
};


struct FileInfo {
  std::string queue;
  std::string path;
};

FileInfo fileinfo(std::string const& filename);

