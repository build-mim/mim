#define _GNU_SOURCE

#include "include/mim.hh"

#include <stdio.h>
#include <unistd.h>
#include <dlfcn.h>
#include <stdarg.h>

#define MAX_MSG_LEN 256

extern "C" { 

void (*original_error)(int status, int errnum, const char *format, ...) = NULL;
void error(int status, int errnum, const char *format, ...) {
  if (!original_error)
    original_error = (typeof(original_error))dlsym(RTLD_NEXT, "error");

  if (errnum & MIM_ERROR_MASK) {
    original_error(status, 0, "Mim: error %d when building artifact",
				   errnum & ~MIM_ERROR_MASK);
  } else {
	char buffer[MAX_MSG_LEN + 1];
    va_list args;
    va_start(args, format);
    int r = vsnprintf(buffer, MAX_MSG_LEN, format, args);
    va_end(args);
    original_error(status, errnum, buffer);
  }
}

char const* (*original_strerror)(int errnum) = NULL;
char const* strerror(int errnum) {
  if (!original_strerror)
    original_strerror = (typeof(original_strerror))dlsym(RTLD_NEXT, "strerror");

  if (errnum & MIM_ERROR_MASK) {
    return "Mim: error when building artifact";
  } else {
    return original_strerror(errnum);
  }
}

}
