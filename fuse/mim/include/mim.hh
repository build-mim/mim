#include <string>

//! Mim errors are indicated by this mask.
#define MIM_ERROR_MASK 256

namespace mim {

#define MIM_OK 0
#define MIM_NOTMIMFILE -1
static const int OK = 0;
static const int NOT_MIMFILE = -1;


// Behaviour: Update a mimfile (if needed).
//   filename    the name of the file to retrieve the command for.
//   result      an int where the exit status of the build process will be
//               available after the function has been called.
// Returns:
//   OK          if the file was updated correctly.
//   NOT_MIMFILE if the file is not a mim artifact.
int update(const char* filename, int& result);

// Behaviour: Retrieve the command used to build a mimfile.
// Arguments:
//   filename    the name of the file to retrieve the command for.
//   command     a std::string where the command will be available after the
//               function has been called.
//   symbolic    true if the command should be returned as entered in the mimfile
//               false if the command should be returned as executed in the shell
// Returns:
//   OK          if the file was updated correctly.
//   NOT_MIMFILE if the file is not a mim artifact.
int command(const char* filename, std::string& command, bool symbolic);


// Behaviour: Retrieve the name of the file that contains 'filenames's stderr
// from its last build.
// Arguments:
//   filename    the name of the file to retrieve the command for.
//   stderrFile  a std::string where the filename will be available after the
//               function has been called.
// Returns:
//   OK          if the file was updated correctly.
//   NOT_MIMFILE if the file is not a mim artifact.
int stderr(const char* filename, std::string& stderrFile);


}
