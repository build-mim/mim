#ifndef MIM_HH
#define MIM_HH


#include <boost/interprocess/ipc/message_queue.hpp>

namespace bi = boost::interprocess;


namespace mim {


//! Get a string that is per-process-unique.
std::string messageQueueName(std::string const& mimQ);

class Mim {
  std::string clientQueueName;
  bi::message_queue responseQ;
  bi::message_queue requestQ;
public:
  Mim(std::string const& clientQueueName, std::string const& serverQueueName);

  std::string command(std::string const& filename);

  ~Mim();
};


struct FileInfo {
  std::string queue;
  std::string path;
};

FileInfo fileinfo(std::string const& filename);
}

#endif
