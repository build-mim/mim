import unittest
import os
import shutil
import subprocess
import sys
import time
import dircache

FILES = {
	"mimfile":r"""
function ls()
  return {
	       { "hello.txt", "./program > %output" },
	       { "program", "/usr/bin/g++ generated-source.cc -L./printer -lprinter -o %output" },
	       { "generated-source.cc", "cat original-source.cc | sed -e \"s/Greetings/`cat greeting.txt`/g\" > %output" },
	       { "greeting.txt", "echo -n Hello old friend > %output" },
	     }
end
""",


	"original-source.cc":r"""
void printer(const char* text);

int main() {
  printer("Greetings");
}""",


"printer/mimfile":r"""
function ls()
  return {
	       { "printer.o", "/usr/bin/g++ -c printer/printer.cc -o %output" },
	       { "libprinter.a", "ar rcs %output printer/printer.o" },
	     }
end
""",


	"printer/printer.cc":r"""
#include <stdio.h>

void printer(const char* text) {
  printf("\"%s\" says a compiled program!\n", text);
}"""

}

TEMPDIR = "/tmp/mim_test/"
MOUNTPOINT = "/tmp/mim_mountpoint/"
class IntegrationTest(unittest.TestCase):
    def setUp(self):
        subprocess.call(["fusermount", "-q", "-u", MOUNTPOINT])
        if os.path.exists(TEMPDIR):
            shutil.rmtree(TEMPDIR)
        if os.path.exists(MOUNTPOINT):
            shutil.rmtree(MOUNTPOINT)
        os.mkdir(MOUNTPOINT)

        for filename, contents in FILES.items():
            dirname = TEMPDIR + os.path.dirname(filename)
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            open("/tmp/mim_test/" + filename, 'w').write(contents)

        self.stdout = open("/tmp/mim.stdout", 'w')
        self.stderr = open("/tmp/mim.stderr", 'w')
        self.mimproc = subprocess.Popen(["bin/mim", "-d", TEMPDIR, MOUNTPOINT], stdout=self.stdout, stderr=self.stderr)
        while not os.path.exists(MOUNTPOINT + "mimfile"):
			time.sleep(0)

    def tearDown(self):
        if not subprocess.call(["fusermount", "-q", "-u", MOUNTPOINT]):
            for x in xrange(0, 10):
                if self.mimproc.poll():
                    break
                time.sleep(0.1)
        else:
			print("Failed to unmount: " + MOUNTPOINT)


    def test_list_files(self):
         rootfiles = set(dircache.listdir(MOUNTPOINT))
         self.assertEquals(set(['generated-source.cc', 'greeting.txt',
                                'hello.txt', 'mimfile', 'original-source.cc',
                                'printer', 'program']),
                           rootfiles)

         printerfiles = set(dircache.listdir(MOUNTPOINT + "printer"))
         self.assertEquals(set(['libprinter.a', 'printer.o', 'printer.cc', 'mimfile']),
                           printerfiles)

    def build(self):
        lines = open(MOUNTPOINT + "hello.txt", 'r').readlines()
        self.assertEquals(['"Hello old friend" says a compiled program!\n'], lines)

    def assertNotBuilds(self):
        try:
            open(MOUNTPOINT + "hello.txt", 'r').readlines()
        except IOError as e:
            self.assertEquals(768, e.errno)
            return
        self.fail("Built successfully, but shouldn't do so.")

    def test_changing_dependency_to_errornous_code_should_result_in_build_failure(self):
        f = open(MOUNTPOINT + "original-source.cc", 'a')
        self.build()
        f.seek(0)
        f.write(FILES["original-source.cc"].replace("printer", "_printer"))
        f.close()
        self.assertNotBuilds()

    def test_renaming_file_to_dependency_should_trigger_update(self):
        self.build()

        f = open(MOUNTPOINT + "original-source2.cc", 'w')
        f.write(FILES["original-source.cc"].replace("printer", "_printer"))
        os.rename(MOUNTPOINT + "original-source2.cc", MOUNTPOINT + "original-source.cc")

        self.assertNotBuilds()

    def test_truncating_dependency_should_trigger_update(self):
        f = open(MOUNTPOINT + "original-source.cc", 'a')
        self.build()

        os.ftruncate(f.fileno(), 0)
        f.close()

        self.assertNotBuilds()


if __name__ == '__main__':
    unittest.main()

